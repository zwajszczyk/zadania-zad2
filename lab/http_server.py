# -*- encoding: utf-8 -*-

import socket
import email.utils
import datetime
import urlparse


def http_serve(server_socket, html):
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()

        try:
            # Odebranie żądania
            request = connection.recv(1024)
            if request:

                print "Odebrano:"
                print request

                requesttable = request.split("\r\n")
                first_line_req = requesttable[0].split(" ")
                if len(first_line_req)==3:
                    uri = first_line_req[1]
                    print uri
                    if (str(first_line_req[0])=="GET") and (str(first_line_req[2][:4])=="HTTP"):
                        # Wysłanie zawartości strony
                        #robimy naglowki
                        header_status = "HTTP/1.1 200 OK\r\n"
                        header_contenttype="Content-Type: text/html; charset=UTF-8\r\n"
                        header_date ="GMT-Date: "+email.utils.formatdate(usegmt=True)+"\r\n"
                        header_length="Content-Length: "+ str(len(html))+"\r\n"
                        print "dobre naglowki"
                        print str(first_line_req[0])
                        print str(str(first_line_req[2][:4]))
                    else:
                        header_status = "HTTP/1.1 400 Bad Request\r\n"
                        header_contenttype="Content-Type: text/html; charset=UTF-8\r\n"
                        header_date ="GMT-Date: "+email.utils.formatdate(usegmt=True)+"\r\n"
                        html = "Uzytkowniku, jestes bardzo zlym czlowiekiem. To jest serwer HTTP i przyjmuje zadania HTTP. Prosimy do okienka nr 4b."
                        header_length="Content-Length: "+ str(len(html))+"\r\n"
                        print "zle naglowki"
                        print str(first_line_req[0])
                        print str(first_line_req[2][:4])
                else:
                    header_status = "HTTP/1.1 400 Bad Request\r\n"
                    header_contenttype="Content-Type: text/html; charset=UTF-8\r\n"
                    header_date ="GMT-Date: "+email.utils.formatdate(usegmt=True)+"\r\n"
                    html = "Uzytkowniku, jestes bardzo zlym czlowiekiem. To jest serwer HTTP i przyjmuje zadania HTTP. Prosimy do okienka nr 4b."
                    header_length="Content-Length: "+ str(len(html))+"\r\n"
                    print "zle naglowki"



                connection.sendall(header_status+header_contenttype+header_date+header_length+"\r\n"+html)

        finally:
            # Zamknięcie połączenia
            connection.close()


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego użycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Powiązanie gniazda z adresem
server_address = ('localhost', 80)  # TODO: zmienić port!
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)

html = open('web/web_page.html').read()

try:
    http_serve(server_socket, html)

finally:
    server_socket.close()

